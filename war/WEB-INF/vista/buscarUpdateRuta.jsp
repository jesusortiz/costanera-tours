<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Buscar Ruta para Actualizar</title>
</head>
<body>
<p>Ingrese el origen y destino de la ruta a  actualizar</p>
<form action="/buscarUpdateRuta">
<label for="origen">Origen: </label><input type="text" name="origen">
<label for="destino">Destino: </label><input type="text" name="destino">
<input type="submit" value="Buscar para Actualizar">
</form>
</body>
</html>