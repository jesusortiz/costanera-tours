<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@page import="Model.Persona" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Actualizar</title>
</head>
<body>
<%if( getServletContext().getAttribute("adminUpdate") !=null){ 
	
	Persona p = (Persona)getServletContext().getAttribute("adminUpdate");

%>
<div class="RegistroAdmin">
<form action="/updateAdmin" method="post">
<label for="name">Nombre: </label><input type="text" name="name" id="name" value="<%=p.getName()%>">
<label for="last">Last: </label><input type="text" name="last" id="last" value="<%=p.getLast()%>">
<label for="email">Email: </label><input type="text" name="email" id="email" value="<%=p.getEmail()%>">
<label for="dni">DNI: </label><input type="text" name="dni" id="dni" value="<%=p.getDni()%>">
<label for="password">Password: </label><input type="text" name="password" id="password" >
<input type="submit" value="Registrar"> 
</form>
</div>
<a href="/listAdmin">>>Ver lista de Administradores</a>

<%}else{%>
<p>No hay conincidencias</p>
<%}%>
</body>
</html>