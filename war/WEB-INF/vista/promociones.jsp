<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    <%@page import="Model.Persona" %>
<%@page import="Model.Metodos" %>
<%@page import="Model.Publicacion" %>
<%@page import="java.util.ArrayList" %>
<%@page import="java.util.List" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Promociones</title>
</head>
<body>
<table>


<% if( (List<Publicacion>)getServletContext().getAttribute("promociones") != null 
&& !((List<Publicacion>)getServletContext().getAttribute("promociones")).isEmpty()){
	
	
	List<Publicacion> publicacion=(List<Publicacion>)getServletContext().getAttribute("promociones");
	
	for(int i=publicacion.size();i>0;i--){
		Publicacion publicaciones=publicacion.get(i-1);
%>
<tr><td>
<div class="center" id="center">
<label><%=publicaciones.getnamePublicacion() %></label>
<p id="post" ><%=publicaciones.getDescription()%></p><br>

<%if(!publicaciones.getnameArchivo().equals("vacio")){

	//este condicional verificara  las publicaciones con archivos %>
    <a href="/serve?blob-key=<%=publicaciones.getKeyArchivo()%>"><%=publicaciones.getnameArchivo()%></a><br>
    <%if(publicaciones.getTypeArchivo().equals("image")){%>
         <img alt="imagen" src="/serve?blob-key=<%=publicaciones.getKeyArchivo()%>" width="auto" height="300px">
         
    <%}else if(publicaciones.getTypeArchivo().equals("video")){%>
         <video src="/serve?blob-key=<%=publicaciones.getKeyArchivo()%>" width="465px" height="350px" controls="controls"></video>
        
    <%}else if(publicaciones.getTypeArchivo().equals("audio")){%>
         <audio src="/serve?blob-key=<%=publicaciones.getKeyArchivo()%>"   controls  ></audio>
        
    <%}else{%>
         <embed src="/serve?blob-key=<%=publicaciones.getKeyArchivo()%>"  width="460px" height="300px" >
         
</div> 
</td></tr>
<%}
    }
}
	}else{%>
	<tr><td>No hay ninguna promocion realizada por los administradores</td></tr>
	<%} %>

</table>
</body>
</html>