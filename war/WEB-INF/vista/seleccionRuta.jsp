<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ page import="Model.Ruta"%>
      <%@ page import="Model.Metodos"%>
       <%@ page import="Model.Horario"%>
       <%@ page import="java.util.List"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="/css/reserva.css">
<link rel="stylesheet" type="text/css" href="/css/tcal.css" />
<script type="text/javascript" src="/js/tcal.js"></script>
<script type="text/javascript" src="/js/ventaHora.js"></script>

<title>Selecion de Ruta y Salida Programada</title>
</head>
<body>
<%if(getServletContext().getAttribute("seleccionRuta") !=null){
	Ruta r = (Ruta)getServletContext().getAttribute("seleccionRuta");
	Metodos m = new Metodos();
	Horario h = m.getHorario(r.getHorarioSalida());
	List<String> horarios= h.getHorarios();
	%>
<label>Horarios de Salida  Para la ruta </label>
<label><%=r.getOrigen()%>-<%=r.getDestino()%></label>
<select name="horaS" >
<%for(String horario: horarios){ %>
<option value="<%=horario%>"><%=horario%></option>
<%} %>

</select>


<!-- venta de Pasajes -->
<label for="venta">Venta de Pasajes</label>
<table>
		<tr>
			<td></td>
			<td></td>
			<td><div class="asiento"><p>1</p></div></td>
			<td><div class="asiento"><p>2</p></div></td>
		</tr>
		<tr>
			<td><div class="asiento"><p>3</p></div></td>
			<td><div class="asiento"><p>4</p></div></td>
			<td></td>
			<td><div class="asiento"><p>5</p></div></td>
		</tr>
		<tr>
			<td><div class="asiento"><p>6</p></div></td>
			<td><div class="asiento"><p>7</p></div></td>
			<td></td>
			<td><div class="asiento"><p>8</p></div></td>
		</tr>
		<tr>
			<td><div class="asiento"><p>9</p></div></td>
			<td><div class="asiento"><p>10</p></div></td>
			<td></td>
			<td><div class="asiento"><p>11</p></div></td>
		</tr>
		<tr>
			<td><div class="asiento"><p>12</p></div></td>
			<td><div class="asiento"><p>13</p></div></td>
			<td><div class="asiento"><p>14</p></div></td>
			<td><div class="asiento"><p>15</p></div></td>
			
		</tr>
</table>

<%}else{ %>
<p>no hay salida programadas aun</p>
<%} %>
</body>
</html>