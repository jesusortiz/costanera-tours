<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html >
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="/css/tcal.css" />
<script type="text/javascript" src="/js/tcal.js"></script>
<title>Crear Salida </title>

</head>
<body>
<h3>Ingrese los siguientes datos para la programacion de la salida</h3>
<!-- Formulario para  la programacion de las salidas -->
<form action="/createSalida" method="post">

<label for="origen">Origen: </label><input type="text" name="origenS" required>
<label for="destino">Destino: </label><input type="text" name="destinoS" required>
<div id="sal"><label for="salida">Fecha de Salida: </label><input type="text" name="fechaS" class="tcal" value=""  readonly />
</div>
<label for="horaS">Hora de Salida: </label><input type="text" name="horaS" required>
<br>
<label for="minivanM">Modelo Minivan: </label><input type="text" name="minivanS" required>
<label for="choferS">Chofer: </label><input type="text" name="choferS" required>
<input type="submit" value="Enviar">
		
</form>
</body>
</html>