<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="ISO-8859-1"%>
<%@page import="Model.Salida;" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Busqueda de Salidas</title>
</head>
<body>
<% 

if((Salida)getServletContext().getAttribute("salida") != null){
	Salida salida = (Salida)getServletContext().getAttribute("salida");

%>
<table>
<tr>
<th>Origen</th>
<th>Destino</th>
<th>Fecha de Viaje</th>
<th>Hora de Viaje</th>
<th>Conductor</th>
<th>Modelo Minivan</th>
</tr>
<tr> 
<td><%=salida.getOrigen()%></td>
<td><%=salida.getDestino()%></td>
<td><%=salida.getFecha()%></td>
<td><%=salida.getHora()%></td>
<td><%=salida.getConductor()%></td>
<td><%=salida.getMinivan().getModelo()%></td>
</tr>

</table>
<% }else{
	
%><p>Horario de Salida no encontrado</p> <%	
}%>

</body>
</html>