<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ page import="Model.Ruta"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

</head>
<body>
<%if(getServletContext().getAttribute("updateRuta") !=null){ 
Ruta ruta = (Ruta)getServletContext().getAttribute("updateRuta");
%>
<p>Actualizar Ruta</p>
<form action="/updateRuta" method="post">
<label for="origen">Origen: </label><input type="text" name="origen"  value="<%=ruta.getOrigen()%>" required>
<label for="destino">Destino: </label><input type="text" name="destino"  value="<%=ruta.getDestino()%>"  required>
<label for="tiempoViaje">Tiempo de Viaje: </label><input type="text" name="tiempoViaje"  value="<%= ruta.getTiempoViaje()%>" required>
<label for="costo">Costo: </label><input type="text" name="costo"  value="<%= ruta.getCosto()%>" required>
<input type="submit" value="Update">
</form>
<%}else{ %>
<p>No hay nada Para actualizar</p>
<%} %>
</body>
</html>