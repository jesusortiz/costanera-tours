<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html >
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="/css/tcal.css" />
<script type="text/javascript" src="/js/tcal.js"></script>
<script type="text/javascript" src="/js/reserva.js"></script>
<script type="text/javascript" src="/js/hora.js"></script>
<link rel="stylesheet" type="text/css" href="/css/reserva.css">

<title>Reserva de Pasajes</title>
</head>

<body>

	<form action="#">
		
		<label>Origen: </label><input type="text" name="origen"><br>
		<label>Destino:</label><input type="text" name="destino"><br>
		<label>Salidas: </label> 
		<select name="salida" onclick="idaVuelta()" id="salida">
			<option value="ida">Solo Ida</option>
			<option value="idaRetorno">Ida y Vuelta</option>
		</select>
		<br>
		
		<div id="sal"><label for="salida">Fecha de Salida: </label><input type="text" name="dateIda" class="tcal" value=""  readonly />
		</div>
		<br>
		<div id="ret">
			<label for="retorno">Fecha de retorno: </label><input type="text" name="dateVuelta" class="tcal" value="" readonly>
		</div>
		<br>
		<label for="horas">Hora de Salida</label> 
		<select name="horaSalida"	id="horaS">
			<option value="3:30">3:30</option>
			<option value="7:30">7:30</option>
			<option value="8:30">8:30</option>
			<option value="9:30">9:30</option>
			<option value="10:30">10:30</option>
			<option value="11:30">11:30</option>
			<option value="12:30">12:30</option>
			<option value="14:30">14:30</option>
			<option value="15:30">15:30</option>
			<option value="16:30">16:30</option>
			<option value="17:30">17:30</option>
			<option value="18:30">18:30</option>
			<option value="19:30">19:30</option>
		</select> 
		<input type="submit" value="Reservar">
	</form>

	<table>
		<tr>
			<td></td>
			<td></td>
			<td><div class="asiento"><p>1</p></div></td>
			<td><div class="asiento"><p>2</p></div></td>
		</tr>
		<tr>
			<td><div class="asiento"><p>3</p></div></td>
			<td><div class="asiento"><p>4</p></div></td>
			<td></td>
			<td><div class="asiento"><p>5</p></div></td>
		</tr>
		<tr>
			<td><div class="asiento"><p>6</p></div></td>
			<td><div class="asiento"><p>7</p></div></td>
			<td></td>
			<td><div class="asiento"><p>8</p></div></td>
		</tr>
		<tr>
			<td><div class="asiento"><p>9</p></div></td>
			<td><div class="asiento"><p>10</p></div></td>
			<td></td>
			<td><div class="asiento"><p>11</p></div></td>
		</tr>
		<tr>
			<td><div class="asiento"><p>12</p></div></td>
			<td><div class="asiento"><p>13</p></div></td>
			<td><div class="asiento"><p>14</p></div></td>
			<td><div class="asiento"><p>15</p></div></td>
			
		</tr>
	</table>


</body>
</html>