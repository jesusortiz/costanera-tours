<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="Model.Ruta"%>
<%@ page import="Model.Metodos"%>
<%@ page import="java.util.List"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Lista de Rutas</title>
</head>
<body>


<%
Metodos m= new Metodos();
if(m.rutas() != null && !m.rutas().isEmpty()){
	List<Ruta>  rutas=m.rutas();
%>
<table border="1" style="width:100%">
<tr>
<th>Origen</th>
<th>Destino</th>
<th>Tiempo de Viaje</th>
<th>Costo s/.</th>
<th>Horario</th>
</tr>

<%	
	for(Ruta ruta : rutas ){
%>	

<tr>
<td><%=ruta.getOrigen()%></td>
<td><%=ruta.getDestino()%></td>
<td><%=ruta.getTiempoViaje()%></td>
<td><%=ruta.getCosto()%></td>
<%if(ruta.getHorarioSalida() != null){ %>
<td><a href="buscarHorario?ID_horario=<%=ruta.getHorarioSalida()%>"> Editar Horario</a></td>
<%}else{ %>
<td><a href="addHorario?origen=<%=ruta.getOrigen()%>&destino=<%=ruta.getDestino()%>">Añada un Horario para la ruta</a></td>
<%}%>
</tr>
<%} %>
</table>
<% 	
}else{
%>
<p>No hay ninguna ruta en el sistema</p>
<%}%>


</body>
</html>