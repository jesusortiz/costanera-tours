<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ page import="Model.Ruta"%>
<%@ page import="Model.Metodos"%>
<%@ page import="java.util.List"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="/js/seleccionRuta.js"></script>

<title>Rapidos Vip</title>
</head>
<body>
<h4>Bienvenido Admin</h4>
<%
Metodos m = new Metodos();
if(m.isRuta()){ 
List<Ruta> rutas =m.getRutas();

%>

<!-- formulario para la busqueda de salidas -->
<form action="/selecionRuta" method="post">

<label for="Origen" >Salida: </label>
<select name="origen" id="origen">
<%for(Ruta ruta: rutas){ %>
<option value="<%=ruta.getOrigen()%>"><%=ruta.getOrigen()%></option>
<%} %>

</select>

<label for="destino">Destino: </label>
<select name="destino" id="destino">
<%for(Ruta ruta: rutas){ %>
<option value="<%=ruta.getDestino()%>"><%=ruta.getDestino()%></option>
<%} %>
</select>
<button type="submit" onclick="seleccion()" >Buscar horario para la salida de la ruta</button>

</form>
<!-- 
<div id="sal"><label for="salida">Fecha de Salida: </label><input type="text" name="fechaS" class="tcal" value=""  readonly />
</div>
<input type="submit"  value="Buscar Salida">
</form>
<br><br>
 -->
<a href="/publicar">Hacer Publicaciones</a><br>
<a href="/crearSalida">Crear Salida</a><br>
<a href="/adminHorarios?">AdministrarHorarios para la salida</a>
<a href="/adminRecursoSalida">Administar Recursos para la salida</a><br>

<a href="/addRuta">Añadir Ruta</a>


<%}else{ %>
<p>EL sistema no cuenta con ninguna ruta en su base de datos usted tiene que añadir una ruta 
y un horario para la ruta</p>
<a href="/addRuta">Añadir Ruta</a>
<%} %>
</body>
</html>