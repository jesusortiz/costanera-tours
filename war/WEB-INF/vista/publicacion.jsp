<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
       
<%@page import="com.google.appengine.api.blobstore.BlobstoreServiceFactory"%>
<%@page import="com.google.appengine.api.blobstore.BlobstoreService" %>
<%@page import="com.google.appengine.api.blobstore.BlobKey" %>
<%@page import="Model.Persona" %>
<%@page import="Model.Metodos" %>
<%@page import="Model.Publicacion" %>
<%@page import="java.util.ArrayList" %>
<%@page import="java.util.List" %>

<%
BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();					
%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Publicaciones</title>
<link rel="stylesheet" href="css/fontello.css">
       <link rel="stylesheet"  href="css/basemenu.css">
       <link rel="stylesheet" href="css/basebanner.css">
       <link rel="stylesheet" href="css/formcss.css">
       <link rel="stylesheet" href="css/basecss.css">



<script type="text/javascript">
function requerir(){
	var archivo=document.getElementById("funcionArchivo");
	archivo.required = "required";
	archivo.disabled=false;
		
}
function noRequerir(){		
	 document.getElementById("funcionArchivo").disabled=true;
}


</script>
<script type="text/javascript">
	 
	/**
	 * este escrib enviara y recibira las publicaciones cuando e
	 * la pagina principal sdis se cargue
	 */
	function getHttpObject(){
		var XMLHttp;
		if (window.XMLHttpRequest) {
			XMLHttp = new XMLHttpRequest();
			
		}else if(window.ActiveXObject) {
			XMLHttp = new ActiveXObject("Microsoft.XMLHTTP");
			window.alert("este navegador obsoleto no soporta esta aplicacion  le sugerimos utilizar google chrome");
			
		}
		return XMLHttp;
	}

	var http = getHttpObject();

	function eliminar(longDelete){
		if(http){
			
			if(longDelete != undefined){
				var ID = longDelete;
				http.onreadystatechange = function(){ 
					//var combo = document.getElementById('combo');
		            var contenido = document.getElementById('contenido');
		            if(http.readyState == 1){    
		                //combo.length = 0;
		                //var nuevaOpcion = document.createElement("option");
		                //nuevaOpcion.value = 0; 
		                //nuevaOpcion.innerHTML="Cargando...";
		                //combo.appendChild(nuevaOpcion); 
		                //combo.disabled = true;
		            	
		            	
		            }
		            
		      if(http.readyState==4 && http.status == 200){
		        //contenido.innerHTML = http.responseText;
		        //combo.disabled = false;
		        window.alert("los datos han sido enviados");
		      }
		    }

			    http.open("GET","/deletePublicacion?ID="+ID,true);
			    http.send();
			}else{
				window.alert("ID indefinido")
			}
		
			
		}	 


	  }
	
	

</script>
</head>
<body>
<!-- formulario para los posteos -->

<section id="t01">
<div class="contenedor">
<table>
<tr><td>
<label for="posteos">Haga sus Promociones</label><br>
<form action="<%= blobstoreService.createUploadUrl("/publicacion") %>" method="post" enctype="multipart/form-data"  class="center">
<label id="head_text">TITULO:</label><br><textarea name="tituloP" id="head_text" placeholder="Publicacion" required title="la publicacion necesita un titulo"></textarea><br>
<textarea  name="descriptionP" placeholder="Haga sus publicaciones" id="publication" required title="añada una publicacion"></textarea>
<label id="head_text">Desea añadir archivos?</label><br>
<input type="radio"  name="addArchivo" value="si" id="aceptar" onclick="requerir()" title="es necesario subir un archivo" ><label>si</label><br>
<input type="radio" name="addArchivo" value="no" id="negar" checked="checked" onclick="noRequerir()" ><label>no</label><br>
<input type="file" name="archivo"  id="funcionArchivo" class="add_archivos" disabled ><br>
<button type="submit" id="formboton">Publicar</button>

</form></td></tr>



<% if( (List<Publicacion>)getServletContext().getAttribute("publicacion") != null  && !((List<Publicacion>)getServletContext().getAttribute("publicacion")).isEmpty()){
	
	
	List<Publicacion> publicacion=(List<Publicacion>)getServletContext().getAttribute("publicacion");
	
	for(int i=publicacion.size();i>0;i--){
		Publicacion publicaciones=publicacion.get(i-1);
%>
<tr><td>
<div class="contenerdor">
<label><%=publicaciones.getnamePublicacion() %></label>
<p id="post" ><%=publicaciones.getDescription()%></p><br>
<button onclick="eliminar(<%=publicaciones.getLongkey()%>)">Eliminar Publicacion</button>
<%if(!publicaciones.getnameArchivo().equals("vacio")){

	//este condicional verificara  las publicaciones con archivos %>
    <a href="/serve?blob-key=<%=publicaciones.getKeyArchivo()%>"><%=publicaciones.getnameArchivo()%></a><br>
    <%if(publicaciones.getTypeArchivo().equals("image")){%>
         <img alt="imagen" src="/serve?blob-key=<%=publicaciones.getKeyArchivo()%>" width="auto" height="300px">
         <button onclick="eliminar(<%=publicaciones.getLongkey()%>)">Eliminar Publicacion</button>
    <%}else if(publicaciones.getTypeArchivo().equals("video")){%>
         <video src="/serve?blob-key=<%=publicaciones.getKeyArchivo()%>" width="465px" height="350px" controls="controls"></video>
         <button onclick="eliminar(<%=publicaciones.getLongkey()%>)">Eliminar Publicacion</button>
    <%}else if(publicaciones.getTypeArchivo().equals("audio")){%>
         <audio src="/serve?blob-key=<%=publicaciones.getKeyArchivo()%>"   controls  ></audio>
         <button onclick="eliminar(<%=publicaciones.getLongkey()%>)">Eliminar Publicacion</button>
    <%}else{%>
         <embed src="/serve?blob-key=<%=publicaciones.getKeyArchivo()%>"  width="460px" height="300px" >
         <button onclick="eliminar(<%=publicaciones.getLongkey()%>)">Eliminar Publicacion</button>
</div> 
</td></tr>
<%}
    }
}
	}%>

</table>
</div>
</section>
</body>
</html>