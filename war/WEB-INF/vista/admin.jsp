<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ page import="Model.Ruta"%>
<%@ page import="Model.Metodos"%>
<%@ page import="java.util.List"%>
<!DOCTYPE html>

<html>
   <head>
       <title>
       Adminitrador
          
       </title>
       <meta charset="UTF-8">
       <meta name="viewport" content="width=device-width,user-scalable=no, initial-scale=1 , maximum-scale=1,minimum-scale=1">
       <link rel="stylesheet" href="css/basecss.css">
       <link rel="stylesheet" href="css/fontello.css">
       <link rel="stylesheet"  href="css/basemenu.css">
       <link rel="stylesheet" href="css/basebanner.css">
       <link rel="stylesheet" href="css/formcss.css">
       <link rel="stylesheet" href="css/baseblog.css">
       <link rel="stylesheet" href="css/baseinfo.css">
       
       <script type="text/javascript" src="/js/seleccionRuta.js"></script>

      
       
       
   </head>
    <!--  <body>
       <header>
           <div class="contenedor">
               <h1 class="icon-lock-open-alt"> Administrador</h1>
               <input type="checkbox" id="menu-bar">
               <label class="icon-menu" for="menu-bar">icono</label>
               <nav class="menu">
                  
                  
                  <a href="destinos.html">Destinos</a>
                  <a href="horaios.html">Horarios</a>
                  <a href="index.html"   class="icon-out"></a>
                  
                   
               </nav>
               
           </div>
       </header>
       <main>
       <section id="blog">
        <h3>Administrar Cuentas</h3>
        <div class="contenedor">
          <article>
          <form action="">
            <h2>Informacion de Cuenta</h2>
            <input type="text" name="infocuenta" placeholder="user@gmail.com" required="Ingrese la cuenta a Buscar">
            <input type="submit" id="formboton" value="Buscar">
            <h2>Resulatdo de busqueda</h2>
            <input type="text" name="nombre" placeholder="Nombres">
            <input type="text" name="apellidos" placeholder="Apellidos">
            <input type="text" name="dni" placeholder="dni">
            <input type="text" name="password" placeholder="password">
            <textarea name="historial" id="user" cols="49" rows="10" >Historial de Viajes </textarea>
          </form>
            
          </article>
          
          <article>
            <form action="">
              <h2>Modificar Cuenta</h2>
              <input type="text" name="user" placeholder="user@gmail.com" required="Ingrese la cuenta a modificar">
              <input type="text" name="nombre" placeholder="Nombres" required="Cmabiar Nombre?"> 
       <input type="text" name="apellido" placeholder="Apellidos" required="Cambiar Apellido">
       <input type="text" name="dni" placeholder="Dni" required="Cambia DNI">
         
         <input type="password" name="password" placeholder="contraseña" required="Cambiar Contraseña">

          
              <input type="submit" id="formboton" vlue="Modificar">
            </form>
          </article>
          <article>
          <form action="">
            <h2>Eliminar Cuenta</h2>
            <input type="text" name="infocuenta" placeholder="user@gmail.com" required="Ingrese la cuenta a Eliminar">
            <input type="submit" id="formboton" value="Eliminar">
           
          </form>
           
          </article>
        </div>
      </section>

        <section id="rutas" >

        <div class="contenedor">
        <h2>Aministrar Publicaciones</h2>
        <form>
        
          <label class="icon-etiqueta">Añadir Titulo<input type="text" name="texto" placeholder="Titulo"> 


           <label class="icon-folderblack">Añadir Imagen<input type="file" name="texto" > </label>


          <p class="icon-foto">Añadir Descripcion </p>
          <textarea name="descripcion" id="descripcion" cols="49" rows="10" placeholder="Ingresa la Descripcion de la Publicacion"></textarea>
          <input type="submit" id="formboton" value="Publicar">

      
       
       
</form>
          


        </div>
        
          </section>
          <section id="rutas">
          <h2>Administracion de Rutas</h2>
            <form action="">
              <p>Origen<input type="text" name="origen" placeholder="origen"></p>
              <p>Destino<input type="text" name="destino" placeholder="destino"></p>
              <p>Fecha<input type="text" name="fecha" placeholder="fecha"></p>
              <p>Hora<input type="text" name="hora" placeholder="hora"></p>
              <p>Costo<input type="text" name="costo" placeholder="costo"></p>
              <input type="submit"  id="formboton" value="Añadir">
              </form>
              <h2>Eliminar ruta</h2>

               <form action="">
              <p>Origen<input type="text" name="origen" placeholder="origen"></p>
              <p>Destino<input type="text" name="destino" placeholder="destino"></p>
              <p>Fecha<input type="text" name="fecha" placeholder="fecha"></p>
              <p>Hora<input type="text" name="hora" placeholder="hora"></p>
              <p>Costo<input type="text" name="costo" placeholder="costo"></p>
              <input type="submit" id="formboton" value="Eliminar">
            </form>
          </section>
        
           
        

       </main>
        
       <footer >
          <div class="contenedor">
          <p class="copy"> MY PAGINA &copy; 2016 </p>
          <div class="sociales">
          <a class ="icon-facebook" href="#"></a>
          <a class="icon-twitter" href="#"></a>
          <a class="icon-instagram" href="#"></a>
          <a class="icon-gplus" href="#"></a>
          </div>
          </div>
        </footer>
        
        
    </body>-->
    
    <body>
<h4>Bienvenido Admin</h4>
<%Metodos m = new Metodos();
if(m.isRuta()){ 
List<Ruta> rutas =m.getRutas();

%>

<!-- formulario para la busqueda de salidas -->
<form action="/selecionRuta" method="post">

<label for="Origen" >Salida: </label>
<select name="origen" id="origen">
<%for(Ruta ruta: rutas){ %>
<option value="<%=ruta.getOrigen()%>"><%=ruta.getOrigen()%></option>
<%} %>

</select>

<label for="destino">Destino: </label>
<select name="destino" id="destino">
<%for(Ruta ruta: rutas){ %>
<option value="<%=ruta.getDestino()%>"><%=ruta.getDestino()%></option>
<%} %>
</select>
<button type="submit" onclick="seleccion()" >Buscar horario para la salida de la ruta</button>

</form>
<!-- 
<div id="sal"><label for="salida">Fecha de Salida: </label><input type="text" name="fechaS" class="tcal" value=""  readonly />
</div>
<input type="submit"  value="Buscar Salida">
</form>
<br><br>
 -->
<a href="/publicar">Hacer Publicaciones</a><br>
<a href="/crearSalida">Crear Salida</a><br>
<a href="/adminHorarios?">AdministrarHorarios para la salida</a>
<a href="/adminRecursoSalida">Administar Recursos para la salida</a><br>

<a href="/addRuta">Añadir Ruta</a>


<%}else{ %>
<p>EL sistema no cuenta con ninguna ruta en su base de datos usted tiene que añadir una ruta 
y un horario para la ruta</p>
<a href="/addRuta">Añadir Ruta</a>
<%} %>
</body>
</html>