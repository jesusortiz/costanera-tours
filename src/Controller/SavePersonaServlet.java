package Controller;

import java.io.IOException;

import javax.jdo.PersistenceManager;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Model.Persona;
import Model.Metodos;
import Model.PMF;

@SuppressWarnings("serial")
public class SavePersonaServlet extends HttpServlet {
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {

		final PersistenceManager pm = PMF.get().getPersistenceManager();
		if (req.getParameter("name") != null && !req.getParameter("name").equals("") && 
				req.getParameter("last") != null &&  !req.getParameter("last").equals("")&& 
				req.getParameter("email") != null && !req.getParameter("email").equals("") &&
				req.getParameter("dni") != null && !req.getParameter("dni").equals("")&&
				req.getParameter("password")!=null && !req.getParameter("password").equals("")) {

			try {
				
				
				Metodos m = new Metodos();
				
				if(!(m.DNIRepeat(req.getParameter("dni")) )){
					
					Persona persona = new Persona(req.getParameter("name"), req.getParameter("last"), req.getParameter("email"),
							req.getParameter("dni"),req.getParameter("password"), "normal");
					
					

					pm.makePersistent(persona);
					HttpSession misesion= req.getSession(true);
					misesion.setAttribute("dni",persona.getDni());
					misesion.setAttribute("name",persona.getName());
					misesion.setAttribute("last",persona.getLast());
					misesion.setAttribute("email",persona.getEmail());
					misesion.setAttribute("tipo", persona.getTipo());
					
					RequestDispatcher rd = req.getRequestDispatcher("/WEB-INF/vista/exito.jsp");
					rd.forward(req, resp);

				}else{
					resp.sendRedirect("/index.html");
				}
				
								
			} catch (Exception e) {

				System.out.println(e);
				resp.getWriter().println("Ocurrio un error, vuelva a intentarlo.");
				resp.sendRedirect("/index.html");

			} finally {
				pm.close();
			}

			
		} else {
			System.out.println("ocurrio un error parametros nulos");
			resp.sendRedirect("/registro.html");
		}

	}
	

}
