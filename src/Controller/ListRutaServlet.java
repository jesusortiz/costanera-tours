package Controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Model.Metodos;
import Model.Ruta;

@SuppressWarnings("serial")
public class ListRutaServlet extends HttpServlet {
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {

		RequestDispatcher rd = null;
		List<Ruta> rutas = null;
		Metodos m = new Metodos();
		try {
			
			if (!m.rutas().isEmpty() && m.rutas() != null) {
				rutas = m.rutas();
				getServletContext().setAttribute("listRutas", rutas);
				rd = req.getRequestDispatcher("/WEB-INF/vista/listRuta.jsp");
				rd.forward(req, resp);

			}else{
			
					
				rd = req.getRequestDispatcher("/WEB-INF/vista/listRuta.jsp");
				rd.forward(req, resp);
			}
			
			

		} catch (Exception e) {
			resp.getWriter().println("Ocurrio  un error, vuelva a intentarlo.");
			e.printStackTrace();
			System.out.println(e.getMessage());
		}

	}

	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		doPost(req, resp);
	}

}
