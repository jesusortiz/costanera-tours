package Controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Model.Metodos;
import Model.PMF;
import Model.Persona;
import Model.Publicacion;

@SuppressWarnings("serial")
public class PromocionesServlet extends HttpServlet{
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException,ServletException{
		
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Query q = pm.newQuery(Publicacion.class);	
		try{
			Metodos m = new Metodos();
			//tarea para etica profesional
			//amparo medina ONU 
			//patricia sandoval
			//sacar parametro dni de la sesion
			List<Publicacion> pub= (List<Publicacion>) q.execute(Publicacion.class);
		
		
		    		    
		    
		    if(!pub.isEmpty() && pub!=null){   	
		    	 
		 	    getServletContext().setAttribute("promociones",pub);
		    }
		  
		   
		    
		    
			RequestDispatcher rd= req.getRequestDispatcher("/WEB-INF/vista/promociones.jsp");
		    rd.forward(req, resp);
			
			
		

			
		}catch(Exception e){
			System.out.println(e.getMessage());
		}finally{
			q.closeAll();
			pm.close();
		}
	}
}
