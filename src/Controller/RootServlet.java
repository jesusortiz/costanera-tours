package Controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class RootServlet extends HttpServlet{
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException{
		
		try {
			RequestDispatcher rd ;
			
			if(req.getParameter("action_root")!=null && !req.getParameter("action_root").trim().equals("")){
				
				String actionRoot = req.getParameter("action_root");
				
				switch (actionRoot) {
				case "createAdmin":
					rd = req.getRequestDispatcher("/WEB-INF/vista/createAdmin.html");
					rd.forward(req, resp);
					break;
					
                case "deleteAdmin":
                	rd = req.getRequestDispatcher("/WEB-INF/vista/deleteAdmin.html");
					rd.forward(req, resp);
					
					break;
				case "updateAdmin":
					rd = req.getRequestDispatcher("/WEB-INF/vista/buscarUpdateAdmin.html");
					rd.forward(req, resp);
					
					break;
					
                case "viewAdmin":
                	rd = req.getRequestDispatcher("/listAdmin");
					rd.forward(req, resp);
					
					break;
					
				default:
					rd = req.getRequestDispatcher("/WEB-INF/vista/root.jsp");
					rd.forward(req, resp);
					
					break;
				}
				
				
			//en caso de que los parametros para ejecutar las acciones de el root sean invalidos	
			}else{
				rd = req.getRequestDispatcher("/WEB-INF/vista/root.jsp");
				rd.forward(req, resp);
				
			}
			//En caso de un error 
		} catch (Exception e) {
			// TODO: handle exception
			RequestDispatcher rd = req.getRequestDispatcher("/WEB-INF/vista/root.jsp");
			rd.forward(req, resp);
		}
	}
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException,ServletException{
		doPost(req,resp);
	}
	
}
