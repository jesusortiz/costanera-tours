package Controller;

import java.io.IOException;

import javax.jdo.PersistenceManager;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Model.Metodos;
import Model.PMF;
import Model.Persona;
@SuppressWarnings("serial")
public class DeleteAdminServlet extends  HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException{
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		try {
			if(req.getParameter("dni")!=null && !req.getParameter("dni").trim().equals("")){
				Metodos m = new Metodos();
				String dni = req.getParameter("dni");
				Long l =m.getPersona(dni).getId();
				
				Persona tutorial = pm.getObjectById(Persona.class,l );
				pm.deletePersistent(tutorial);
				RequestDispatcher rd = req.getRequestDispatcher("/WEB-INF/vista/listAdmin.jsp");
				rd.forward(req, resp);
			}else{
				
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("error");
		}
		
		
		
		
		
	}

}
