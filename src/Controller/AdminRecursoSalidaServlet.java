package Controller;

import java.io.IOException;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Model.Minivan;
import Model.PMF;
import Model.Persona;
@SuppressWarnings("serial")
public class AdminRecursoSalidaServlet extends HttpServlet{
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException,ServletException{
		
		
		try {
			
			PersistenceManager pm = PMF.get().getPersistenceManager();
			
			Query q = pm.newQuery(Minivan.class);
			List<Minivan> minivanes = (List<Minivan>) q.execute(Minivan.class);
			
			if(minivanes != null && !minivanes.isEmpty()){
				getServletContext().setAttribute("ListMinivanes", minivanes);
			}else{
				
			}
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
}
