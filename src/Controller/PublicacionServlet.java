package Controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Model.Metodos;
import Model.Persona;
import Model.Publicacion;
@SuppressWarnings("serial")
public class PublicacionServlet extends HttpServlet  {
	public void  doGet(HttpServletRequest req, HttpServletResponse resp)
			throws  IOException, ServletException{
		
		try{
			Metodos m = new Metodos();
			//tarea para etica profesional
			//amparo medina ONU 
			//patricia sandoval
			//sacar parametro dni de la sesion
			HttpSession misesion = req.getSession();
			if(misesion !=null  && misesion.getAttribute("dni")!=null && !(((String)misesion.getAttribute("dni")).trim().equals(""))){
				String dni = (String)misesion.getAttribute("dni");
				if(m.getPersona(dni)!=null){
					Persona p = m.getPersona(dni);
					
					 List<Publicacion> publicacion = new ArrayList<Publicacion>();		    
					    
					    if(!p.getLongPublicaciones().isEmpty() && p.getLongPublicaciones()!=null){
					    	
					    	for(int i=0;i<p.getLongPublicaciones().size();i++){
						    	publicacion.add(m.getPublicacion(p.getLongPublicaciones().get(i)));
						    	
						    }
					    	 
					 	    getServletContext().setAttribute("publicacion",publicacion);
					 	    RequestDispatcher rd= req.getRequestDispatcher("/WEB-INF/vista/publicacion.jsp");
						    rd.forward(req, resp);
					    }
					    
				}else{
					resp.sendRedirect("loginAdmin.html");
				}
				
			}else{
				resp.sendRedirect("loginAdmin.html");
			}
		    
	

			
		}catch(Exception e){
			System.out.println(e.getMessage());
			resp.sendRedirect("index.html");
			
		}
		
	}
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException{
		doGet(req, resp);
		
	}
		
}
