package Controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class AdminHorariosSalidaServlet extends HttpServlet{
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException,ServletException{
		
		try {
			RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/vista/listRuta.jsp");
			dispatcher.forward(req, resp);
		
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
}
