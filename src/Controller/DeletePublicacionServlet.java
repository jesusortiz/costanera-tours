package Controller;

import java.io.IOException;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Model.Metodos;
import Model.PMF;
import Model.Persona;
import Model.Publicacion;
@SuppressWarnings("serial")
public class DeletePublicacionServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException{
		
		if(req.getParameter("ID")!=null && !req.getParameter("ID").trim().equals("")){
			
			resp.setContentType("text/plain");		
			final PersistenceManager pm = PMF.get().getPersistenceManager();
			final Metodos m= new Metodos();
			final Query q = pm.newQuery(Publicacion.class);
			
				try{
					//creamos la variable para eliminar  el ID
					Long ID = Long.parseLong(req.getParameter("ID"));
					//eliminamos la publicacion de la base de datos
					
					//obtenemos la persona el dni sacamos de la sesion
					Persona p = m.getPersona("76193593");
					
					//eliminamos el ID que la persona contiene en el arrayList
					List<Long> listaPublicacionesLong = p.getLongPublicaciones();
					int index=0;
					for(Long longP :p.getLongPublicaciones()){
						if(longP.equals(ID)){
							p.getLongPublicaciones().remove(index);
							
							break;
						}
						index++;
					}
					
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/publicar");
					dispatcher.forward(req, resp);
					
				}catch(Exception e){
						System.out.println(e);
						resp.getWriter().println("No se han podido borrar datos.");
						resp.sendRedirect("/index.html");
				}finally{
					q.closeAll();
					pm.close();
				}
		}else{
			resp.getWriter().println("ID invalida de la publicacion");
		}
		
	}

}
