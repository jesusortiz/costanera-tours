package Controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@SuppressWarnings("serial")
public class AdminServlet extends HttpServlet{
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException{
		//evalua si el ingreso del codigo del admin no esta vacia
		if(req.getParameter("codigo")!=null && !req.getParameter("codigo").trim().equals("")){
			//evalua si el ingreso del codigo del admin coincide con Perricle$39
			if(req.getParameter("codigo").equals("admin")){
				try{
					
					
					//en caso de que el admin se indentifique redirecciona a una pagina
					RequestDispatcher rd = req.getRequestDispatcher("/WEB-INF/vista/root.jsp");
					rd.forward(req, resp);
					
					
					
				}catch(Exception e){
					//en caso de un error redirecciona  a la pagina principal
					resp.sendRedirect("index.html");
				}
		    //en caso de que no se ha introducido la contraseña correctamente
			}else{
				resp.sendRedirect("index.html");
			}
		//en caso de que no se ha  introducido  ninguna contraseña	
		}else{
			resp.sendRedirect("index.html");
		}
		
	}

	
	

}
