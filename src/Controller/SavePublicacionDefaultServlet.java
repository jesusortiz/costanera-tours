package Controller;

import java.io.IOException;

import javax.jdo.PersistenceManager;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Model.PMF;
import Model.Publicacion;
@SuppressWarnings("serial")
public class SavePublicacionDefaultServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException{
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Publicacion publicacion= new Publicacion("blob_key_vacio", "SIVEPAT", "BIENVENIDO A SIVEPAT","vacio","type_file_vacio");
		pm.makePersistent(publicacion);
		pm.close();
		
		RequestDispatcher rd= getServletContext().getRequestDispatcher("/WEB-INF/vista/root.jsp");
	 	rd.forward(req, resp);
	
		
	}

}
