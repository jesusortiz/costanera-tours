package Controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Model.Metodos;
import Model.Persona;

@SuppressWarnings("serial")
public class ListAdminServlet extends HttpServlet {
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {

		RequestDispatcher rd = null;
		List<Persona> persona = null;
		Metodos m = new Metodos();
		try {
			
			if (!m.listAdmin().isEmpty() && m.listAdmin() != null) {
				persona = m.listAdmin();
				getServletContext().setAttribute("listAdmin", persona);
				rd = req.getRequestDispatcher("/WEB-INF/vista/listAdmin.jsp");
				rd.forward(req, resp);

			}else{
			
					
				rd = req.getRequestDispatcher("/WEB-INF/vista/createAdmin.html");
				rd.forward(req, resp);
			}
			
			

		} catch (Exception e) {
			resp.getWriter().println("Ocurrio  un error, vuelva a intentarlo.");
			e.printStackTrace();
			System.out.println(e.getMessage());
		}

	}

	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		doPost(req, resp);
	}

}
