package Controller;

import java.io.IOException;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Model.Metodos;
import Model.PMF;
import Model.Ruta;

@SuppressWarnings("serial")
public class SaveRutaServlet extends HttpServlet{
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException,ServletException{
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		try {
			if(req.getParameter("origen")!=null && !req.getParameter("origen").equals("") &&
					req.getParameter("destino")!= null && !req.getParameter("destino").equals("")&&
				    req.getParameter("tiempoViaje")!=null &&  !req.getParameter("tiempoViaje").equals("")&&
					req.getParameter("costo")!=null && !req.getParameter("costo").equals("")){
				
				String origen = req.getParameter("origen");
				String destino = req.getParameter("destino");
				String tiempoViaje = req.getParameter("tiempoViaje");
				String costo = req.getParameter("costo");
				if(!new Metodos().rutaRepetida(origen, destino) && !origen.equals(destino)){
					Ruta ruta = new Ruta(origen, destino, tiempoViaje, costo);
					pm.makePersistent(ruta);
					
					Metodos m=  new Metodos();
					List<Ruta> rutas = m.rutas();
					getServletContext().setAttribute("listRutas",rutas);
					
					RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/vista/listRuta.jsp");
					dispatcher.forward(req, resp);
				}else{
					RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/vista/crearRuta.jsp");
					dispatcher.forward(req, resp);
				}
				
			
				
			}else{
				RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/vista/crearRuta.jsp");
				dispatcher.forward(req, resp);
			}
			
			
			
			
		
		} catch (Exception e) {
			// TODO: handle exception
		}finally{
			pm.close();
		}
	}
}