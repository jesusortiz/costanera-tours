package Controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Model.Metodos;
import Model.Ruta;


@SuppressWarnings("serial")
public class SeleccionRutaServlet extends HttpServlet{
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException,ServletException{
		try {
			if(req.getParameter("origen") !=null && !req.getParameter("origen").trim().equals("")
					&& req.getParameter("destino")!=null && !req.getParameter("destino").trim().equals("")
					){
				
				
				String  origen = req.getParameter("origen");
				String destino = req.getParameter("destino");
				Metodos m = new Metodos();
				if(m.getRuta(origen, destino)!=null){
					Ruta r =m.getRuta(origen, destino);
					getServletContext().setAttribute("seleccionRuta",r);
					RequestDispatcher rd = req.getRequestDispatcher("/WEB-INF/vista/seleccionRuta.jsp");
			        rd.forward(req, resp);
				}else{
					RequestDispatcher rd = req.getRequestDispatcher("/WEB-INF/vista/adminPag.jsp");
			        rd.forward(req, resp);
				}
				
			}else{
				RequestDispatcher rd = req.getRequestDispatcher("/WEB-INF/vista/adminPag.jsp");
		        rd.forward(req, resp);
			}
				
		} catch (Exception e) {
			// TODO: handle exception
			RequestDispatcher rd = req.getRequestDispatcher("/WEB-INF/vista/pagAdmin.jsp");
	        rd.forward(req, resp);
		}
		
	}
}
	


