package Controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.jdo.PersistenceManager;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.appengine.api.blobstore.BlobInfo;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;

import Model.Metodos;
import Model.PMF;
import Model.Persona;
import Model.Publicacion;
@SuppressWarnings("serial")
public class SavePublicacionServlet extends HttpServlet  {
	 private BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
	 
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException{
		
		
		 try {
			 
				
			 Map<String, BlobKey> blobs = blobstoreService.getUploadedBlobs(req);
		     Map<String, List<BlobInfo>> blobInfo= blobstoreService.getBlobInfos(req);		     
		     
		     List<BlobInfo>listBlobInfo=null;
		     BlobKey blobKey =null;
		
		     PersistenceManager pm = PMF.get().getPersistenceManager();
		     Publicacion publicacion=null;
		     List<Publicacion>publicaciones = null;
		     
		     if(req.getParameter("tituloP") != null 	&& !req.getParameter("tituloP").trim().equals("") &&
						req.getParameter("descriptionP") !=null && !req.getParameter("descriptionP").trim().equals("")&&
						
						req.getParameter("addArchivo") !=null && !req.getParameter("addArchivo").trim().equals("")){
					
					String titulo = req.getParameter("tituloP");
					String descripcion = req.getParameter("descriptionP");
					
					String addArchivo = req.getParameter("addArchivo");
					//verificamos si el admin quiere  o no a�adir archivos a su publicacion
					if( addArchivo.equals("si")){
						blobKey=blobs.get("archivo");
						listBlobInfo=blobInfo.get("archivo"); //esto almacenara en una lista los datos de _blobInfo_
						String archivoKey = blobKey.getKeyString();
						String nameArchivo="falta nombre";	
						
						if(blobKey != null && !listBlobInfo.isEmpty()&& listBlobInfo != null && !archivoKey.equals("")&& archivoKey!=null ){
								
								nameArchivo=listBlobInfo.get(0).getFilename();
								String typefile=listBlobInfo.get(0).getContentType();
								String typeFile=typefile.substring(0,typefile.indexOf("/"));
								publicacion = new Publicacion(archivoKey,titulo,descripcion,nameArchivo,typeFile);
								
								pm.makePersistent(publicacion);
								pm.close(); 
						 	    Metodos m= new Metodos();
						 	    
						 	   
						 	    HttpSession misesion = req.getSession();
								if(misesion !=null  && misesion.getAttribute("dni")!=null && !(((String)misesion.getAttribute("dni")).trim().equals(""))){
									String dni = (String)misesion.getAttribute("dni");
									if(m.getPersona(dni)!=null){
										Persona p = m.getPersona(dni);
										m.addPublication(p.getDni(),publicacion.getLongkey());
								 		publicaciones = new ArrayList<Publicacion>();		    
										    
								 		if(getServletContext().getAttribute("publicacion") != null ){
								 			publicaciones = (List<Publicacion>)getServletContext().getAttribute("publicacion");
								 			publicaciones.add(publicacion);

											  
										    RequestDispatcher rd= req.getRequestDispatcher("/WEB-INF/vista/puentePublicaciones.html");
										    rd.forward(req, resp);
								 			
								 			
								 		}else{
								 			for(int i=0;i<p.getLongPublicaciones().size();i++){
										    	publicaciones.add(m.getPublicacion(p.getLongPublicaciones().get(i)));
										    	
										    	
										    }
									 	    getServletContext().setAttribute("publicacion",publicacion);	

											  
										    RequestDispatcher rd= req.getRequestDispatcher("/WEB-INF/vista/puentePublicaciones.html");
										    rd.forward(req, resp);
								 		}
										 
										    
									}else{
										resp.sendRedirect("loginAdmin.html");
									}
									
								}else{
									resp.sendRedirect("loginAdmin.html");
								}
						 	    
						 	   
								
							}else{
								resp.sendRedirect("/");
							}
						}else if(addArchivo.equals("no")){
							
							publicacion= new Publicacion("blob_key_vacio", titulo, descripcion,"vacio","type_file_vacio");
							pm.makePersistent(publicacion);
							
								
							Metodos m= new Metodos();
							Persona p=m.getPersona("76193593");
							
						 	m.addPublication(p.getDni(),publicacion.getLongkey());
							pm.close();
							
							if(getServletContext().getAttribute("publicacion") != null ){
					 			publicaciones = (List<Publicacion>)getServletContext().getAttribute("publicacion");
					 			publicaciones.add(publicacion);
					 			
					 		}else{
					 			for(int i=0;i<p.getLongPublicaciones().size();i++){
							    	publicaciones.add(m.getPublicacion(p.getLongPublicaciones().get(i)));
							    	
							    }
						 	    getServletContext().setAttribute("publicacion",publicacion);	
					 		}
						 	RequestDispatcher rd= req.getRequestDispatcher("/WEB-INF/vista/puentePublicaciones.html");
						 	rd.forward(req, resp);
						 	
						
						}else{
							RequestDispatcher rd= req.getRequestDispatcher("/WEB-INF/vista/publicacion.jsp");
						 	rd.forward(req, resp);
						//si no coincide las opciones para addArchivo
					}
					
		     }else{
		    	 //reponde a la pagina de la publicacion
		    	 System.out.println("parametros nulos");
			 }
			 
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
			e.printStackTrace();
			resp.sendRedirect("/");
			
		}finally{
			
		}	
		
		
		
	}


}
