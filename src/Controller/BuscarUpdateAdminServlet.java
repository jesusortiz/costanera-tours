package Controller;


import java.io.IOException;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Model.Metodos;
import Model.PMF;
import Model.Salida;

@SuppressWarnings("serial")
public class BuscarUpdateAdminServlet extends HttpServlet{
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException{
		
		if(req.getParameter("dni") !=null && !req.getParameter("dni").trim().equals("")){
			
			
			String dni = req.getParameter("dni");
			Metodos m = new Metodos();
			
			if(m.getPersona(dni) !=null){
				getServletContext().setAttribute("adminUpdate", m.getPersona(dni));
				RequestDispatcher rd= req.getRequestDispatcher("/WEB-INF/vista/updateAdmin.jsp");
				rd.forward(req, resp);
				
			}else{
				RequestDispatcher rd= req.getRequestDispatcher("/WEB-INF/vista/buscarUpdateAdmin.html");
				rd.forward(req, resp);
			}
			


			
			
			
		}else{
			System.out.println("datos para la  busqueda invalidos");
		}
		
	}

}