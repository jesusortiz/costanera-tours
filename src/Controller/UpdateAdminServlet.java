package Controller;

import java.io.IOException;

import javax.jdo.PersistenceManager;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Model.Persona;
import Model.Metodos;
import Model.PMF;

@SuppressWarnings("serial")
public class UpdateAdminServlet extends HttpServlet {
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {

		final PersistenceManager pm = PMF.get().getPersistenceManager();
		if (req.getParameter("name") != null && !req.getParameter("name").equals("") && 
				req.getParameter("last") != null &&  !req.getParameter("last").equals("")&& 
				req.getParameter("email") != null && !req.getParameter("email").equals("") &&
				req.getParameter("dni") != null && !req.getParameter("dni").equals("")&&
				req.getParameter("password")!=null && !req.getParameter("password").equals("")) {

			try {
				String dni = req.getParameter("dni");
				String last = req.getParameter("last");
				String name = req.getParameter("name");
				String email = req.getParameter("email");
				String password = req.getParameter("password");
				Metodos m = new Metodos();
				if(!(m.DNIRepeat(req.getParameter("dni")) )){
					if(getServletContext().getAttribute("updateAdmin")!=null){
						Persona persona =(Persona)getServletContext().getAttribute("updateAdmin");
						Persona temp = m.getPersona(persona.getDni());
						temp.setDni(dni);
						temp.setEmail(email);
						temp.setLast(last);
						temp.setName(name);
						temp.setPassword(password);
						RequestDispatcher rd = req.getRequestDispatcher("/WEB-INF/vista/listAdmin.jsp");
						rd.forward(req, resp);
					}else{
						RequestDispatcher rd = req.getRequestDispatcher("/WEB-INF/vista/buscarUpdateAdmin.html");
						rd.forward(req, resp);
					}
					

					

				}else{
					RequestDispatcher rd = req.getRequestDispatcher("/WEB-INF/vista/index.html");
					rd.forward(req, resp);
				}
				
								
			} catch (Exception e) {

				System.out.println(e);
				resp.getWriter().println("Ocurrio un error, vuelva a intentarlo.");
				RequestDispatcher rd = req.getRequestDispatcher("/WEB-INF/vista/index.html");
				rd.forward(req, resp);

			} finally {
				pm.close();
			}

			
		} else {
			System.out.println("ocurrio un error parametros nulos");
			resp.sendRedirect("/registro.html");
		}

	}
	

}
