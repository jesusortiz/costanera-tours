package Controller;

import java.io.IOException;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Model.Boleto;
import Model.PMF;
import Model.Persona;

@SuppressWarnings("serial")
public class ReservaPasajeServlet extends HttpServlet {
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
	throws IOException{
		

		try{
			final PersistenceManager pm = PMF.get().getPersistenceManager();
			final Query q = pm.newQuery(Boleto.class);
			
			List<Boleto> boletos = (List<Boleto>) q.execute(Boleto.class);

			for (Boleto b : boletos) {


			}
			

			
			if(req.getParameter("salida") !=null && !req.getParameter("salida").equals("")){
				String salida= req.getParameter("salida");
				
				if(salida.equals("ida")){
					if(req.getParameter("day") != null && !req.getParameter("day").equals("") 
							&& req.getParameter("dateIda") != null && !req.getParameter("dateIda").equals("") 
							&& req.getParameter("origen")!=null && !req.getParameter("origen").equals("")
							&& req.getParameter("destino")!=null && !req.getParameter("destino").equals("")
							&& req.getParameter("horaSalida")!=null &&  !req.getParameter("horaSalida").equals("")){
						
					}
					
				}else if(salida.equals("idaRetorno")){
					if(req.getParameter("day") != null && !req.getParameter("day").equals("") 
							&& req.getParameter("dateIda") != null && !req.getParameter("dateIda").equals("") 
							&& req.getParameter("idaVuelta") !=null && !req.getParameter("idaVuelta").equals("")
							&& req.getParameter("origen")!=null && !req.getParameter("origen").equals("")
							&& req.getParameter("destino")!=null && !req.getParameter("destino").equals("")
							&& req.getParameter("horaSalida")!=null &&  !req.getParameter("horaSalida").equals("")){
						
					}
					
				}
				
			}else{
				resp.sendRedirect("index.html");
			}
			
			
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		
	}

}
