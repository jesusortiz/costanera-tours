package Controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Model.Horario;
import Model.Metodos;
import Model.PMF;
import Model.Ruta;

@SuppressWarnings("serial")
public class CreateHorarioServlet extends HttpServlet{
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException,ServletException{
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		try {
			
			
			if(req.getParameter("origen")!=null && !req.getParameter("origen").trim().equals("") &&
					req.getParameter("destino")!=null && !req.getParameter("destino").trim().equals("") ){
				
				String origen = req.getParameter("origen");
				String destino = req.getParameter("destino");
				ArrayList<String>h = new ArrayList<String >();
				
				Metodos m= new Metodos();
				Horario horarios= new Horario(h);
				
				Ruta r = null;
				if(m.getRuta(origen, destino)!=null && m.getRuta(origen, destino).getHorarioSalida()==null ){
					r = m.getRuta(origen, destino);
					pm.makePersistent(horarios);
					r.setHorarioSalida(horarios.getId());
					RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/vista/listRuta.jsp");
					dispatcher.forward(req, resp);
					
				}
				
				
				
			}else{
				RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/vista/listRuta.jsp");
				dispatcher.forward(req, resp);
			}
			
			
		
		} catch (Exception e) {
			// TODO: handle exception
			RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/vista/listRuta.jsp");
			dispatcher.forward(req, resp);
		}finally{
			pm.close();
		}
	}
}
