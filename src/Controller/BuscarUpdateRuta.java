package Controller;

import java.io.IOException;

import javax.jdo.PersistenceManager;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Model.Metodos;
import Model.PMF;
import Model.Ruta;

@SuppressWarnings("serial")
public class BuscarUpdateRuta extends HttpServlet{
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException,ServletException{
		
		try {
			if(req.getParameter("origen")!=null && !req.getParameter("origen").equals("") &&
					req.getParameter("destino")!= null && !req.getParameter("destino").equals("")){
				String origen = req.getParameter("origen");
				String destino = req.getParameter("destino");
				Metodos m = new Metodos();
				Ruta r = m.getRuta(origen, destino);
				if(r !=null ){
					getServletContext().setAttribute("updateRuta", r);
					RequestDispatcher rd = req.getRequestDispatcher("/WEB-INF/vista/updateRuta.jsp");
					rd.forward(req, resp);
				}else{
					RequestDispatcher rd = req.getRequestDispatcher("/WEB-INF/vista/buscarUpdateRuta.jsp");
					rd.forward(req, resp);
				}
			}else{
				RequestDispatcher rd = req.getRequestDispatcher("/WEB-INF/vista/buscarUpdateRuta.jsp");
				rd.forward(req, resp);
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			RequestDispatcher rd = req.getRequestDispatcher("/WEB-INF/vista/buscarUpdateRuta.jsp");
			rd.forward(req, resp);
		}
	}
}
