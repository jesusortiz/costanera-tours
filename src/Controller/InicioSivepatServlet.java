package Controller;


import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.appengine.api.search.query.QueryParser.restriction_return;

import Model.Metodos;
import Model.Persona;
@SuppressWarnings("serial")
public class InicioSivepatServlet extends HttpServlet {
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException{	
		
		try {
			
			//evalua que los parametos nombre y correo electronico no esten vacias
			if(req.getParameter("dni")!=null  && !req.getParameter("dni").equals("")
					&& req.getParameter("password")!=null && !req.getParameter("password").equals("")){
				
				
				//obtiene los parametros 
				String password= req.getParameter("password");
	            //System.out.println(password);
				String dni= req.getParameter("dni");
				//System.out.println(dni);
				//crea una clase metodos que contiene metodos para autenticar
				Metodos m= new Metodos();
				//cremos la variable de la sesion
				
				//creamos una variable Persona para pasarle los datos a la sesion iniciada
				Persona persona = null;
				
				//Verifica que si la persona que se logea es el admin o es un usuario de la aplicacion
				String type=m.getType(dni,password);
				persona = m.getPersona(dni);
				//System.out.println(type);
			
				if(type.equals("admin")){
					//redirecciona  a la pagina del admin
				
				    HttpSession misesion= req.getSession(true);
				    
					misesion.setAttribute("dni",persona.getDni());
					misesion.setAttribute("nombre",persona.getName());
					misesion.setAttribute("apellido",persona.getLast());
					misesion.setAttribute("email",persona.getEmail());
					misesion.setAttribute("tipo", persona.getTipo());
					RequestDispatcher rd = req.getRequestDispatcher("/WEB-INF/vista/admin.jsp");
					rd.forward(req, resp);
					
				}else if(type.equals("normal")){
					
					//redirecciona a la pagina para los usuarios
					HttpSession misesion= req.getSession(true);
					misesion.setAttribute("dni",persona.getDni());
					misesion.setAttribute("nombre",persona.getName());
					misesion.setAttribute("apellido",persona.getLast());
					misesion.setAttribute("email",persona.getEmail());
					misesion.setAttribute("tipo", persona.getTipo());
					
					RequestDispatcher rd=req.getRequestDispatcher("/indexout.html");
					rd.forward(req, resp);
				}else{
					resp.sendRedirect("/loginuser.html");
				}
			
			}else if(req.getSession()!=null){
				HttpSession miSession =req.getSession();
				
				String dni =(String) miSession.getAttribute("dni");
				Persona p= new Metodos().getPersona(dni);
				RequestDispatcher dispatcher=null;
			    if(p.getTipo().equals("normal")){
					dispatcher= req.getRequestDispatcher("/WEB-INF/vista/reserva.jsp");
					dispatcher.forward(req, resp);
				}else{
					resp.sendRedirect("/");
				}
				
				
			}else{
				
				//en caso de que este vacio ingresa al error sesion
				resp.sendRedirect("/errorIniciarSesion.html");
	
			}
			
		} catch (Exception e) {
			resp.sendRedirect("/errorIniciarSesion.html");
		}
		
		
		
		
		
	}
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException{
		doPost(req,resp);
		
	}
	

}
