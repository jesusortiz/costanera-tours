package Controller;

import java.io.IOException;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Model.PMF;
import Model.Persona;

@SuppressWarnings("serial")
public class ListPersona extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// resp.setContentType("text/plain");

		final PersistenceManager pm = PMF.get().getPersistenceManager();
		final Query q = pm.newQuery(Persona.class);

		if (req.getParameter("curso") != null) {

			String curso = req.getParameter("curso");
			// q.setOrdering("idPersona ascending");
			q.setOrdering("idAlumno descending");
			// q.setRange(0, 10);
			q.setFilter("curso.getName() == cursoParam");
			q.declareParameters("String cursoParam");
			try {

				@SuppressWarnings("unchecked")
				List<Persona> alumnos = (List<Persona>) q.execute(curso);
				req.setAttribute("alumnos", alumnos);
				RequestDispatcher rd = req.getRequestDispatcher("/WEB-INF/listAlumno.jsp");
				rd.forward(req, resp);

			} catch (Exception e) {
				System.out.println(e);
			} finally {
				q.closeAll();
				pm.close();
			}

		} else {
			q.setOrdering("idAlumno descending");
			// q.setRange(0, 10);
			try {
				@SuppressWarnings("unchecked")
				List<Persona> alumnos = (List<Persona>) q.execute();
				req.setAttribute("alumnos", alumnos);
				RequestDispatcher rd = req
						.getRequestDispatcher("/WEB-INF/listAlumno.jsp");
				rd.forward(req, resp);
			} catch (Exception e) {
				System.out.println(e);
			} finally {
				q.closeAll();
				pm.close();
			}
		}
	}
}
