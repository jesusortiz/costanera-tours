package Controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.jdo.PersistenceManager;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Model.Metodos;
import Model.PMF;
import Model.Persona;
import Model.Publicacion;

@SuppressWarnings("serial")
public class SaveAdminServlet extends HttpServlet{
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException{
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		
		if(     req.getParameter("name")!=null && !req.getParameter("name").equals("") &&
				req.getParameter("last")!= null && !req.getParameter("last").equals("")&&
			    req.getParameter("email")!=null &&  !req.getParameter("email").equals("")&&
				req.getParameter("dni")!=null && !req.getParameter("dni").equals("") &&
				req.getParameter("password")!=null && !req.getParameter("password").equals("")){
			
			
			
			try{
					
					RequestDispatcher rd =null;
					Metodos m= new Metodos();
					Integer.parseInt(req.getParameter("dni"));
					if(req.getParameter("dni").length()==8){
					if(!m.DNIRepeat(req.getParameter("dni"))){
						if(m.getNumAdminComplete()){
							
							Persona admin= new Persona(req.getParameter("name"), req.getParameter("last"), req.getParameter("email"),req.getParameter("dni"),req.getParameter("password"),"admin"); 
							
							pm.makePersistent(admin);
							
							
							getServletContext().setAttribute("listAdmin",m.listAdmin());
						
							rd = req.getRequestDispatcher("/WEB-INF/vista/listAdmin.jsp");
							rd.forward(req, resp);
						}else{
							rd = req.getRequestDispatcher("/WEB-INF/vista/registroAdmin.jsp");
							rd.forward(req, resp);
						}
					}else{
						rd = req.getRequestDispatcher("/WEB-INF/vista/registroAdmin.jsp");
						rd.forward(req, resp);
					}

			}					
				}catch(Exception e){
					System.out.println(e);
					resp.getWriter().println("Ocurrio  un error, vuelva a intentarlo.");
					
				}finally{
					pm.close();
				}
			
		
		}else{
			System.out.println("error los parametros estan vacios o estan nulos");
			RequestDispatcher rd = req.getRequestDispatcher("/WEB-INF/vista/registroAdmin.jsp");
			rd.forward(req, resp);
			
		}
		
	}

}
