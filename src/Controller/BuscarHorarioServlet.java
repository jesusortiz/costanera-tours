package Controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class BuscarHorarioServlet extends HttpServlet{
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException,ServletException{
		try {
			if(req.getParameter("ID_horario")!=null && !req.getParameter("ID_horario").trim().equals("")){
				String ID = req.getParameter("ID_horario");
				getServletContext().setAttribute("ID_horario",ID);
				RequestDispatcher rd = req.getRequestDispatcher("/WEB-INF/vista/addHorario.jsp");
				rd.forward(req, resp);
			}else{
				RequestDispatcher rd = req.getRequestDispatcher("/WEB-INF/vista/listRuta.jsp");
				rd.forward(req, resp);
			}
				
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	
	
	}
	
}