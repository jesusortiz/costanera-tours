package Controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import Model.Minivan;
import Model.Salida;
@SuppressWarnings("serial")
public class CreateSalidaServlet extends HttpServlet {
	public  void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException {
		
		if(req.getParameter("fechaS") != null && !req.getParameter("fechaS").trim().equals("") && 
				req.getParameter("horaS") != null && !req.getParameter("horaS").trim().equals("") &&
				req.getParameter("origenS") != null  && !req.getParameter("origenS").trim().equals("") &&
				req.getParameter("destinoS") != null && !req.getParameter("destinoS").trim().equals("") && 
				req.getParameter("choferS") !=null && !req.getParameter("choferS").trim().equals("") &&
				req.getParameter("minivanS") != null && !req.getParameter("minivanS").trim().equals("") &&
				req.getParameter("numAsientos") !=null && !req.getParameter("numAsientos").trim().equals("")){
			
			 
		    
		    try{
		    	
		    	String fechaS = req.getParameter("fechaS");
				String horaS = req.getParameter("horaS");
				String origenS = req.getParameter("origenS");
				String destinoS = req.getParameter("destinoS");
			    String choferS = req.getParameter("choferS");
			 
			    String modelo = req.getParameter("minivanM");
			    int numAsientos =Integer.parseInt( req.getParameter("numAsientos"));
			    
			    Minivan minivan = new Minivan(modelo,numAsientos);
			    RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/vista/crearSalida.jsp");
			    dispatcher.forward(req, resp);
			    Salida salida = new Salida(origenS, destinoS, horaS, fechaS, minivan, choferS);
		    	
		    }catch(NumberFormatException num){
		    	System.out.println("error en el formato");
		    	resp.getWriter().println("numero error");
		    }catch (Exception e) {
				// TODO: handle exception
		    	System.out.println(e.getMessage());
			}
		    	
		}else{
			System.out.println("parametros nulos");
			RequestDispatcher dispatcher =req.getRequestDispatcher("/WEB-INF/vista/crearSalida.jsp");
			dispatcher.forward(req, resp);
		}
		
	}
	public void doGet(HttpServletRequest req, HttpServletResponse resp)throws IOException,ServletException{
		doPost(req,resp);
	}


}
