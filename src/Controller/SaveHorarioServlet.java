package Controller;


import java.io.IOException;
import java.util.ArrayList;

import javax.jdo.PersistenceManager;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Model.Horario;
import Model.Metodos;
import Model.PMF;

@SuppressWarnings("serial")
public class SaveHorarioServlet extends HttpServlet{
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException,ServletException{
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		try {
			if(getServletContext().getAttribute("ID_horario") !=null){
				if(req.getParameter("hora")!=null && !req.getParameter("hora").trim().equals("") &&
						req.getParameter("minuto")!=null && !req.getParameter("minuto").trim().equals("") &&
						req.getParameter("addHora") != null && !req.getParameter("addHora").trim().equals("")){
					
					String hora = req.getParameter("hora");
					String minuto = req.getParameter("minuto");
					String addHora = req.getParameter("addHora");
					String ID =  (String)getServletContext().getAttribute("ID_horario");
					Metodos m= new Metodos();
					
					if(addHora.equalsIgnoreCase("si")){
						if(validacion(Integer.parseInt(hora), Integer.parseInt(minuto))){
							if(m.getHorario(Long.parseLong(ID)) !=null){
								String horaProgramada = hora+ ":"+ minuto;
								Horario h = m.getHorario(Long.parseLong(ID));
								
								if( !(horaRepetida(h.getHorarios(),horaProgramada) ) ){
									
									
									h.addHora(horaProgramada);
									
									//ordenando(h.getHorarios());
									
									
									
									RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/vista/listHorario.jsp");
									dispatcher.forward(req, resp);
								}else{
									RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/vista/listHorario.jsp");
									dispatcher.forward(req, resp);
								}
								
								
								
							}else{
								RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/vista/listRuta.jsp");
								dispatcher.forward(req, resp);
							}
							
						}else{
							System.out.println("hora invalida");
							RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/vista/listHorario.jsp");
							dispatcher.forward(req, resp);
						}
					}else if(addHora.equalsIgnoreCase("no")){
						RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/vista/listHorario.jsp");
						dispatcher.forward(req, resp);
					}else{
						System.out.println("otra opcion  del radio button");
					}
					
					
					
				}else{
					System.out.println("parametros nulos");
					RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/vista/addHorario.jsp");
					dispatcher.forward(req, resp);
				}
				
				
			}else{
				RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/vista/listRuta.jsp");
				dispatcher.forward(req, resp);
			}
			
		
		} catch (Exception e) {
			// TODO: handle exception
			RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/vista/listRuta.jsp");
			dispatcher.forward(req, resp);
		
			
		}finally{
			pm.close();
		}
	}
	public boolean  validacion(int hora , int minuto){
		boolean valido= false;
		if(hora>=0 && hora <= 23 && minuto>=0 && minuto <=59){
			valido = true;
		}
		return valido;
		
	}
	/*public boolean  ordenado(ArrayList<String> horarios){
		boolean ordenado =true;
		try {
			int pivotH;
			int pivotM;
			int H;
			int M;
			int temp;
			if(horarios !=null && !horarios.isEmpty()){
				pivotH=Integer.parseInt(horarios.get(0).substring(0,horarios.lastIndexOf(":")));
				pivotM=Integer.parseInt(horarios.get(0).substring(horarios.lastIndexOf(":")+1));
				for(int i=0;i<horarios.size();i++){
					
					for(int j=i+1;j<horarios.size()-1;j++){
						M=Integer.parseInt(horarios.get(j).substring(0,horarios.lastIndexOf(":")));
						H=Integer.parseInt(horarios.get(j).substring(horarios.lastIndexOf(":")+1));
						if(pivotH>H){
							temp= pivotH;
							
							pivotH=H;
							H=temp;
							
						}else if(pivotH==H && pivotM >M){
							temp=pivotM;
							pivotM= M;
							M= temp;
						}
							
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	*/
	public boolean horaRepetida(ArrayList<String> hora,String horaR){
		boolean horaRepetida= false;
		for(int i=0;i<hora.size();i++){
			if(hora.get(i).equals(horaR)){
				System.out.println("entro a la hora");
				horaRepetida = true;
				break;
			}
				
		}
		return horaRepetida;
	}
}